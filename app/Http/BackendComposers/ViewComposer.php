<?php

namespace App\Http\BackendComposers;

use Illuminate\View\View;
use JeroenNoten\LaravelAdminLte\AdminLte;

class ViewComposer
{
  /**
   * @var AdminLte
   */
  private $adminlte;

  public function __construct(AdminLte $adminlte)
  {
    $this->adminlte = $adminlte;
  }

  public function compose(View $view)
  {
    $view->with('adminlte', $this->adminlte);
  }
}
