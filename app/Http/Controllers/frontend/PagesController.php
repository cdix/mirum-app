<?php

namespace App\Http\Controllers\frontend;

use \App\Category;
use \App\Post;

class PagesController extends FrontendController
{
  public function home()
  {
    $posts      = Post::all();
    $categories = Category::all();

    return view('frontend.pages.home', compact('posts', 'categories'));
  }
}
