<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class FrontendController extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
  /**
   * @var AdminLte
   */
  public function __construct()
  {
  }
}
