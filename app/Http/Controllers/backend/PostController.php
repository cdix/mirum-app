<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use \App\Category;
use \App\Post;

class PostController extends BackendController
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $posts      = Post::all();
    $categories = Category::all();
    return view('backend.post.index', compact('posts', 'categories'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $categories = Category::all();
    return view('backend.post.create', compact('categories'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'category_id'       => 'required',
      'title'             => 'required',
      'slug'              => 'required|unique:posts',
      'short_description' => 'required',
      'content'           => 'required',
      'image'             => 'required',
      // 'thumbnail'         => 'required',
      'publish_on'        => 'required|date',
    ]);

    $post                    = new Post;
    $post->category_id       = $request->category_id;
    $post->user_id           = \Auth::user()->id;
    $post->title             = $request->title;
    $post->slug              = $request->slug;
    $post->short_description = $request->short_description;
    $post->content           = $request->content;
    $post->image             = $request->image;
    $post->publish_on        = $request->publish_on;
    // $post->thumbnail         = $request->thumbnail;

    if ($post->save()) {
      return redirect()->route('post.index')->with('flash-success', 'Data berhasil disimpan.');
    }

    return redirect()->back()->withErrors($post->getErrors())->withInput();return $request->all();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Post $post)
  {
    return view('backend.post.show', compact('post'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Post $post)
  {
    $categories = Category::all();
    return view('backend.post.edit', compact('post', 'categories'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Post $post)
  {
    $post->update($request->all());
    return redirect()->route('post.index')->with('flash-success', 'Data berhasil disimpan.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Post $post)
  {
    if ($post->delete()) {
      return redirect()->route('post.index')->with('flash-success', 'Data berhasil dihapus.');
    } else {
      return redirect()->route('post.index')->with('flash-errors', 'Data gagal dihapus.');
    }
  }
}
