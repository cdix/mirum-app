<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use \App\Category;

class CategoryController extends BackendController
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $categories = Category::all();
    return view('backend.category.index', compact('categories'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('backend.category.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required',
      'slug' => 'required|unique:categories',
    ]);

    $category          = new Category;
    $category->name    = $request->name;
    $category->slug    = $request->slug;
    $category->user_id = \Auth::user()->id;

    if ($category->save()) {
      return redirect()->route('category.index')->with('flash-success', 'Data berhasil disimpan.');
    }

    return redirect()->back()->withErrors($category->getErrors())->withInput();return $request->all();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Category $category)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Category $category)
  {
    return view('backend.category.edit', compact('category'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Category $category)
  {
    $category->update($request->all());
    return redirect()->route('category.index')->with('flash-success', 'Data berhasil disimpan.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Category $category)
  {
    if ($category->delete()) {
      return redirect()->route('category.index')->with('flash-success', 'Data berhasil dihapus.');
    } else {
      return redirect()->route('category.index')->with('flash-errors', 'Data gagal dihapus.');
    }
  }
}
