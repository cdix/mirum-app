<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use \App\User;

class UserController extends BackendController
{
  /**
   * Display change password view
   */
  public function change_password($id)
  {
    $user = User::find($id);
    return view('backend.user.change_password', compact('user'));
  }

  public function save_change_password(Request $request, $id)
  {
    $this->validate($request, [
      'password' => 'required|min:6|confirmed',
    ]);
    $user = User::find($id);
    if (\Hash::check($request->current_password, $user->password) || \Hash::check('password', $user->password)) {
      $user->update(['password' => bcrypt($request->password)]);
      flash('Password Succesfully Changed!');
      return redirect()->route('dashboard');
    } else {
      flash('Current Password is wrong!', 'danger');
      return redirect()->route('change_password', $user->id);
    }
  }
}
