<?php

namespace App\Http\Controllers\backend;

class DashboardController extends BackendController
{
  public function index()
  {
    return view('backend.dashboard.index');
  }
}
