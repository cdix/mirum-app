<?php

namespace App\Http\Controllers\backend;

use Datatables;
use Illuminate\Http\Request;
use \App\Category;
use \App\Post;
use \App\User;

class DatatablesController extends BackendController
{
  /**
   * Process datatables ajax request.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function users()
  {
    return Datatables::of(User::all())
      ->editColumn('is_admin', '@if($is_admin) Admin @else Operator @endif')
      ->make(true);
  }

  public function categories()
  {
    return Datatables::of(Category::query())
      ->editColumn('created_at', function ($data) {return $data->created_at->format('d F Y');})
      ->editColumn('user_id', function ($data) {return $data->user->name;})
      ->addColumn('actions', '<a href="{{ route(\'category.edit\', $slug) }}" class="fa fa-edit btn btn-sm btn-primary">Edit</a>
            {{postLink(route("category.destroy",$slug),"<span class=\'fa fa-trash btn btn-danger btn-sm\'>Delete</span>","Are you sure to delete?")}}')
      ->make(true);
  }

  public function posts()
  {
    return Datatables::of(Post::query())
      ->addColumn('thumbnail', '<img src="/thumbs/{{ $image }}" alt="">')
      ->editColumn('user_id', function ($data) {return $data->user->name;})
      ->addColumn('actions', '<a href="{{ route(\'post.show\', $slug) }}" class="fa fa-eye btn btn-sm btn-default">Show</a>
          <a href="{{ route(\'post.edit\', $slug) }}" class="fa fa-edit btn btn-sm btn-primary">Edit</a>
            {{postLink(route("post.destroy",$slug),"<span class=\'fa fa-trash btn btn-danger btn-sm\'>Delete</span>","Are you sure to delete?")}}')
      ->make(true);
  }
}
