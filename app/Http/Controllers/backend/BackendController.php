<?php

namespace App\Http\Controllers\backend;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use View;

class BackendController extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
  /**
   * @var AdminLte
   */
  public function __construct()
  {
    View::composers([
      'App\Http\BackendComposers\ViewComposer' => '*',
    ]);
  }
}
