<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  protected $fillable = ['category_id', 'title', 'slug', 'short_description', 'content', 'image', 'thumbnail', 'publish_on'];
  protected $dates    = ['publish_on'];

  public function category()
  {
    return $this->belongsTo('App\Category');
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }
}
