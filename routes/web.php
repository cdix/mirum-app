<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();

Route::group(['prefix' => '/', 'middleware' => 'web', 'namespace' => 'frontend'], function () {
  Route::get('/', 'PagesController@home')->name('home');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'backend'], function () {
  Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
  Route::resource('/post', 'PostController');
  Route::resource('/category', 'CategoryController');
  Route::get('/change_password/{id}', 'UserController@change_password')->name('change_password');
  Route::patch('/save_change_password/{id}', 'UserController@save_change_password')->name('save_change_password');

  Route::group(['prefix' => 'datatables'], function () {
    Route::get('/', ['as' => 'datatables.index', 'uses' => 'DatatablesController@index']);
    // Route::get('users', ['as' => 'datatables.users', 'uses' => 'DatatablesController@users']);
    Route::get('categories', ['as' => 'datatables.categories', 'uses' => 'DatatablesController@categories']);
    Route::get('posts', ['as' => 'datatables.posts', 'uses' => 'DatatablesController@posts']);
  });
});

// Route for social login
Route::get('auth/github', 'Auth\LoginController@redirectToProvider');
Route::get('auth/github/callback', 'Auth\LoginController@handleProviderCallback');
