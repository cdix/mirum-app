@extends('layouts.frontend')

@section('content')
<div class="row">
    <!-- Blog Entries Column -->
    <div class="col-md-8">
        <h1 class="page-header">
        My Blog
        <small>Secondary Text</small>
        </h1>
        @foreach ($posts as $post)
        <h2>
        <a href="#">{{ $post->title }}</a>
        </h2>
        <p class="lead">
            Created by {{ $post->user->name }}
        </p>
        <p><span class="glyphicon glyphicon-time"></span> Posted on {{ $post->publish_on->format('d F Y') }}</p>
        <hr>
        <img class="img-responsive" src="/source/{{ $post->image }}" alt="">
        <hr>
        {!! $post->short_description !!}
        <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
        <hr>
        @endforeach
        <!-- Pager -->
        <ul class="pager">
            <li class="previous">
                <a href="#">&larr; Older</a>
            </li>
            <li class="next">
                <a href="#">Newer &rarr;</a>
            </li>
        </ul>
    </div>
    <!-- Blog Sidebar Widgets Column -->
    <div class="col-md-4">
        <!-- Blog Search Well -->
        <div class="well">
            <h4>Blog Search</h4>
            <div class="input-group">
                <input type="text" class="form-control">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                    <span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
            </div>
            <!-- /.input-group -->
        </div>
        <!-- Blog Categories Well -->
        <div class="well">
            <h4>Blog Categories</h4>
            <div class="row">
                <div class="col-lg-6">
                    <ul class="list-unstyled">
                        @foreach ($categories as $category)
                            <li><a href="#">{{ $category->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>
    </div>
</div>
@stop