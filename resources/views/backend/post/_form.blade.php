<div class="form-group has-feedback {{ $errors->has('category_id') ? 'has-error' : '' }}">
    <label for="nama">Category</label>
    <select name="category_id" class="form-control select2">
        <option value="">Pleas Select</option>
        @foreach ($categories as $category)
        <option value="{{ $category->id }}" {{ empty($post) ? '' : $post->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
        @endforeach
    </select>
    @if ($errors->has('category_id'))
    <span class="help-block">
        <strong>{{ $errors->first('category_id') }}</strong>
    </span>
    @endif
</div>
<div class="form-group has-feedback {{ $errors->has('title') ? 'has-error' : '' }}">
    <label for="nama">Title</label>
    <input type="text" name="title" class="form-control" value="{{ empty($post) ? old('title') : $post->title }}" id="post_title">
    @if ($errors->has('title'))
    <span class="help-block">
        <strong>{{ $errors->first('title') }}</strong>
    </span>
    @endif
</div>
<div class="form-group has-feedback {{ $errors->has('short_description') ? 'has-error' : '' }}">
    <label for="short_description">Short Description</label>
    <textarea name="short_description" id="short_description" class="form-control">{{ empty($post) ? old('short_description') : $post->short_description }}</textarea>
    @if ($errors->has('short_description'))
    <span class="help-block">
        <strong>{{ $errors->first('short_description') }}</strong>
    </span>
    @endif
</div>
<div class="form-group has-feedback {{ $errors->has('content') ? 'has-error' : '' }}">
    <label for="content">Content</label>
    <textarea name="content" id="content" class="form-control editor">{{ empty($post) ? old('content') : $post->content }}</textarea>
    @if ($errors->has('content'))
    <span class="help-block">
        <strong>{{ $errors->first('content') }}</strong>
    </span>
    @endif
</div>
<div class="form-group has-feedback {{ $errors->has('image') ? 'has-error' : '' }}">
    <label for='image' class='col-md-2 control-label'>Image</label>
    <input class="form-control required" onfocus="$('.filemanager').trigger('click')" readonly type="text" placeholder="File name" id="image" name="image">
    <div class="input-group-addon">
        <a class="filemanager btn btn-danger" href="{{url('/')}}/filemanager/dialog.php?type=1&field_id=image&relative_url=1"><span class="glyphicon glyphicon-zoom-in"></span></a>
    </div>
    @if ($errors->has('image'))
    <span class="help-block">
        <strong>{{ $errors->first('image') }}</strong>
    </span>
    @endif
</div>
<img src="{{ empty($post) ? '' : '/thumbs/'.$post->image }}" alt="" id="image_preview" style="margin-top:15px;max-height:100px;">
<div class="form-group has-feedback {{ $errors->has('slug') ? 'has-error' : '' }}">
    <label for="nama">Slug</label>
    <input type="text" name="slug" class="form-control" value="{{ empty($post) ? old('slug') : $post->slug }}" id="post_slug">
    @if ($errors->has('slug'))
    <span class="help-block">
        <strong>{{ $errors->first('slug') }}</strong>
    </span>
    @endif
</div>
<div class="form-group has-feedback {{ $errors->has('publish_on') ? 'has-error' : '' }}">
    <label for="publish_on">Publish On</label>
    <input type="text" name="publish_on" class="form-control date" value="{{ empty($post) ? old('publish_on') : $post->publish_on }}">
    @if ($errors->has('publish_on'))
    <span class="help-block">
        <strong>{{ $errors->first('publish_on') }}</strong>
    </span>
    @endif
</div>
<button type="submit" class="btn btn-primary btn-flat">Save</button>

@section('js')
    <script>
        $(document).ready(function() {
            var slug = $('#post_slug');
            $('#post_title').on('blur', function() {
                slug.val(convertToSlug($(this).val()));
            });
        });
    </script>
@stop