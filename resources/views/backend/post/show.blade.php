@extends('layouts.admin')

@section('title', 'Show Post')

@section('content_header')
    <h1>Show Post</h1>
@stop

@section('content')
<div class="col-xs-12"><img src="/source/{{ $post->image }}" alt=""></div>
<h1>{{ $post->title }}</h1>
<h5><span class="glyphicon glyphicon-time"></span> Post by {{ $post->user->name }}, {{ $post->publish_on->format('d F Y') }}.</h5>
<h5><span class="label label-danger">{{ $post->category->name }}</span></h5><br>
{!! $post->content !!}
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop