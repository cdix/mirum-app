@extends('layouts.admin')

@section('title', 'Edit Post')

@section('content_header')
    <h1>Edit Post</h1>
@stop

@section('content')
<form action="{{ route('post.update', $post->slug) }}" method="post">
    {!! csrf_field() !!}
    <input type="hidden" name="_method" value="put">
    @include('backend.post._form')
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop