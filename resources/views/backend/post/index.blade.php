@extends('layouts.admin')

@section('title', 'Posts')

@section('content_header')
    <h1>Data Posts</h1>
@stop

@section('content')
@if($categories->count() > 0)
  <a href="{{ route('post.create') }}" class="btn btn-primary btn-md">Create Post</a>
  <div class="clearfix"></div>
  <br>
@endif
<table class="table table-striped table-hover table-bordered" id="post-table">
  <thead>
    <tr>
      <th>ID Posts</th>
      <th>Title</th>
      <th>Category</th>
      <th>Created By</th>
      <th>Publish On</th>
      <th>Thumbnail</th>
      <th>Created At</th>
      <th></th>
    </tr>
  </thead>
</table>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@push('js')
<script>
$(function() {
    $('#post-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.posts') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'title', name: 'title' },
            { data: 'category_id', name: 'category_id', searchable: false },
            { data: 'user_id', name: 'user_id', searchable: false },
            { data: 'publish_on', name: 'publish_on' },
            { data: 'thumbnail', name: 'thumbnail', searchable: false },
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions', searchable: false }
        ]
    });
});
</script>
@endpush