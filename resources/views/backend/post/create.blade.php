@extends('layouts.admin')

@section('title', 'Create Post')

@section('content_header')
    <h1>Create Post</h1>
@stop

@section('content')
<form action="{{ route('post.store') }}" method="post">
    {!! csrf_field() !!}
    @include('backend.post._form')
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop