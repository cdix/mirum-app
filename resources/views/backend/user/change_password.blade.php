@extends('layouts.admin')

@section('title', 'Change Password')

@section('content_header')
    <h1>Change Password</h1>
@stop

@section('content')
<table class="table table-striped table-hover table-bordered">
  <tr>
    <th>Name</th>
    <td>{{ $user->name }}</td>
  </tr>
  <tr>
    <th>Email</th>
    <td>{{ $user->email }}</td>
  </tr>
</table>
<div class="clearfix"></div>
<br>
<form action="{{ route('save_change_password', $user->id) }}" method="post">
    {!! csrf_field() !!}
    <input type="hidden" name="_method" value="patch">
    @if (!\Hash::check('password', $user->password))
      <div class="form-group has-feedback {{ $errors->has('current_password') ? 'has-error' : '' }}">
          <label for="current_password">Current Password</label>
          <input type="password" name="current_password" class="form-control"
                 placeholder="Current Password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          @if ($errors->has('current_password'))
              <span class="help-block">
                  <strong>{{ $errors->first('current_password') }}</strong>
              </span>
          @endif
      </div>
    @endif
    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
        <label for="password">New Password</label>
        <input type="password" name="password" class="form-control"
               placeholder="New Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
        <label for="password_confirmation">Confirm New Password</label>
        <input type="password" name="password_confirmation" class="form-control"
               placeholder="Confirm New Password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
        @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
    </div>
    <button type="submit" class="btn btn-primary btn-flat">Save</button>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@push('js')
<script>
</script>
@endpush