<div class="col-md-4">
    <div class="form-group has-feedback {{ $errors->has('category') ? 'has-error' : '' }}">
        <label for="category">Name</label>
        <input type="text" name="name" class="form-control" value="{{ empty($category) ? old('name') : $category->name }}" id="category_name">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group has-feedback {{ $errors->has('slug') ? 'has-error' : '' }}">
        <label for="slug">Slug</label>
        <input type="text" name="slug" class="form-control" value="{{ empty($category) ? old('slug') : $category->slug }}" id="category_slug">
        @if ($errors->has('slug'))
            <span class="help-block">
                <strong>{{ $errors->first('slug') }}</strong>
            </span>
        @endif
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</div>

@section('js')
    <script>
        $(document).ready(function() {
            var slug = $('#category_slug');
            $('#category_name').on('blur', function() {
                slug.val(convertToSlug($(this).val()));
            });
        });
    </script>
@stop