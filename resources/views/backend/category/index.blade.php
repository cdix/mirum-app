@extends('layouts.admin')

@section('title', 'Categories')

@section('content_header')
    <h1>Data Categories</h1>
@stop

@section('content')
<a href="{{ route('category.create') }}" class="btn btn-primary btn-md">Create Category</a>
<div class="clearfix"></div>
<br>
<table class="table table-striped table-hover table-bordered" id="category-table">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Slug</th>
      <th>Created By</th>
      <th>Created At</th>
      <th></th>
    </tr>
  </thead>
</table>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@push('js')
<script>
$(function() {
    $('#category-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.categories') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'slug', name: 'slug' },
            { data: 'user_id', name: 'user_id' },
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions', searchable: false }
        ]
    });
});
</script>
@endpush