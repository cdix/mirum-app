@extends('layouts.admin')

@section('title', 'Create Category')

@section('content_header')
    <h1>Create Category</h1>
@stop

@section('content')
<form action="{{ route('category.store') }}" method="post">
    {!! csrf_field() !!}
    @include('backend.category._form')
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
