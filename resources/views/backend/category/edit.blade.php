@extends('layouts.admin')

@section('title', 'Edit Category')

@section('content_header')
    <h1>Edit Category</h1>
@stop

@section('content')
<form action="{{ route('category.update', $category->slug) }}" method="post">
    {!! csrf_field() !!}
    <input type="hidden" name="_method" value="put">
    @include('backend.category._form')
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop