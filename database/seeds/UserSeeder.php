<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $now   = \Carbon\Carbon::now()->toDateTimeString();
    $users = ['name' => 'sidik', 'email' => 'sidik.saepudin13@gmail.com', 'password' => bcrypt('password'), 'is_admin' => true, 'created_at' => $now,
      'updated_at'     => $now];
    DB::table('users')->insert($users);
  }
}
