<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $now        = \Carbon\Carbon::now()->toDateTimeString();
    $categories = [
      [
        'name'       => 'Music',
        'user_id'    => 1,
        'slug'       => 'music',
        'created_at' => $now,
        'updated_at' => $now,
      ],
      [
        'name'       => 'Movie',
        'user_id'    => 1,
        'slug'       => 'movie',
        'created_at' => $now,
        'updated_at' => $now,
      ],
      [
        'name'       => 'Sport',
        'user_id'    => 1,
        'slug'       => 'sport',
        'created_at' => $now,
        'updated_at' => $now,
      ],
    ];
    DB::table('categories')->insert($categories);
  }
}
